;;; to-data-url.el --- generate a data URL -*- lexical-binding: t -*-

;; Author: David Thompson
;; Keywords: convenience multimedia wp AsciiDoc
;; Package-Requires: ((so-long "1.1.2") (too-long-lines-mode "0")
;; Version: 0.2
;; URL: https://codeberg.org/thomp/to-data-url

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Provides functions for generating data URLs. To insert the data URL for a base64-encoded image, place the file path in the kill ring and then use `tdu-insert-as-data-url'.

;;;; Requirements:

(defvar tdu-too-long-lines-missing-p nil)

(require 'so-long)
;; https://github.com/rakete/too-long-lines-mode
(unless (require 'too-long-lines-mode nil 'noerror)
  (setf tdu-too-long-lines-missing-p t)
  (warn "too-long-lines-mode missing")
  )

;;; Code:

(defvar *tdu-supported-formats* '("gif" "png" "svg"))

(defun tdu-long-lines ()
  "Improve long line handling in the current buffer."
  ;; improve Emacs handling of long lines
  (so-long-minor-mode)
  (setf buffer-read-only nil)
  ;; ease burden on viewer
  (unless tdu-too-long-lines-missing-p
    (setf too-long-lines-threshold 1000)
    (too-long-lines-mode)))

(defun tdu-insert-as-data-url (&optional file-path no-line-break-p)
  "Insert the data URL for a base64-encoded image. The file path for
the image should be the first item in the kill ring."
  (interactive)
  (let ((file-path (or file-path
		       (substring-no-properties (cl-first kill-ring)))))
    (tdu-long-lines)
    (let ((base64-string (tdu-base-64-encode-file file-path
                                                   t))
          (messagep nil))
      ;; Insert data URL:  data:[<mediatype>][;base64],<data>
      (insert "data:")
      (insert (mailcap-extension-to-mime (file-name-extension file-path)))
      (insert ";base64,")
      (insert base64-string)
      ;; tll mode won't hide newly-inserted content w/o a prod
      (unless tdu-too-long-lines-missing-p
	(too-long-lines-hide)))))

(defun tdu-uri-to-data-uri ()
  "Return, as a string, the base 64-encoded data URI corresponding to
the next (relative to point) file URI in the current buffer."

  ;; naively assume 'file:/' indicates the start of the next file URI per RFC 8089
  
  ;; search forward for any of supported uri schemes
  ;;   supported:            file
  ;;   should be supported:  ftp,http,sftp
  ;;   unsupported:          info,mailto,news,nntp,snews,rlogin,telnet,irc,nfs,ldap,man
  (when (search-forward "file:/")
    (let ((uri-start (match-beginning 0)))
      (let ((uri-as-string (thing-at-point 'url)))
        (let ((parsed-uri (url-generic-parse-url uri-as-string))
              (uri-end (+ url-start (length uri-as-string))))
          (cond ((string= (url-type parsed-uri) "file")
                 (let ((filename (url-filename parsed-uri)))
                   (assert (member (file-name-extension filename)
                                   *tdu-supported-formats*))
                   (cond ((file-exists-p filename)
                          (list (with-temp-buffer
                                  (insert (format "data:image/png;base64,%s"
                                                  (base-64-encode-file filename)))
                                  (buffer-string))
                                url-start
                                url-end))
                         (t
                          (error "The \"file\" scheme was specified but the indicated file was not found")))))
                ;; FIXME: ideally, handle other url types including NIL and http
                ((member (url-type parsed-url) '("http" "https"))
                 (error "The \"http(s)\" scheme is not yet supported by this function"))
                ))))))
;;
;; Files
;; 
(defun tdu-base-64-encode-file (filename &optional no-line-break-p)
  "Return, as a string, the base 64-encoded representation of the
content of the file specified by FILENAME. NO-LINE-BREAK-P controls
line breaks in base64-encoded content."
  (base64-encode-string
   (with-temp-buffer
     (insert-file-contents filename)
     (buffer-string))
   no-line-break-p))
;;
;; Convenience function(s) for AsciiDoc
;;
(defun tdu-to-ad-data-url ()
  "Insert AsciiDoc markup for a base64-encoded image. The file path
for the image should be the first item in the kill ring."
  (interactive)
  (let ((file-path (substring-no-properties (cl-first kill-ring)))
        ;; The asciidoctor does _not_ like line breaks in base64-encoded content
        ;; - last checked with Asciidoctor 2.0.20 [https://asciidoctor.org]
        (no-line-break-p t))
    (insert #xa "// " file-path #xa)
    ;; Insert asciidoc markup for data URL
    ;;   image::data:[<mediatype>][;base64],<data>
    (insert "image::")
    (tdu-insert-as-data-url file-path no-line-break-p)
    (insert "[]" #xa)))
;;
;; Convenience function(s) for HTML
;;
(defun tdu-html-img-to-data-uri ()
  "Replace next (relative to point) <img ...> tag in buffer with the
base 64-encoded data URL."
  (interactive)
  ;; naively assume "<img", when encountered will represent the start
  ;; of a legitimate IMG tag
  (when (search-forward "<img" nil t)
    ;; a robust version of this would handle both http and file URIs
    (when (search-forward "src=\"")
      (let ((url-start (point)))
        (mark)
        (let ((url-as-string (thing-at-point 'url)))
          (let ((parsed-url (url-generic-parse-url url-as-string))
                (url-end (+ url-start (length url-as-string))))
            (cond ((string= (url-type parsed-url) "file")
                   (let* ((filename (url-filename parsed-url))
                          (file-name-extension (file-name-extension filename)))
                     (assert (member file-name-extension
                                     *tdu-supported-formats*))
                     (assert (file-exists-p filename))
                                        ;(message "The \"file\" scheme was specified but the indicated file was not found")
                     (cond ((member file-name-extension
                                    '("gif" "png"))
                            (goto-char url-end)
                            (kill-region url-start url-end)
                            (insert (format "data:image/%s;base64,%s"
                                            file-name-extension ; bitmap-type
                                            (base-64-encode-file filename))))
                           ((string= file-name-extension "svg")
                            (goto-char url-end)
                            (kill-region url-start url-end)
                            (insert (format "data:image/svg+xml;base64,%s"
                                            (base-64-encode-file filename)))
                            )
                           )))
                  ;; FIXME: ideally, handle other url types including NIL and http
                  ((member (url-type parsed-url) '("http" "https"))
                   (message "The \"http(s)\" scheme is not yet supported"))
                  )))))))

(defun tdu-uri-to-img-elt ()
  "Replace next (relative to point) URI in buffer with an HTML IMG tag
with the corresponding base 64-encoded data URL. Only file scheme is
currently supported."
  (interactive)
  ;; match-spec: (data-uri-string match-start match-end)
  (let ((match-spec (tdu-uri-to-data-uri)))
    (cond (match-spec
           (let ((data-uri-string (elt match-spec 0))
                 (url-start (elt match-spec 1))
                 (url-end (elt match-spec 2)))
             (goto-char url-end)
             (kill-region url-start url-end)
             (insert "<img src=\"")
             (insert data-uri-string)
             (insert "\" >")))
          (t (message "Match on file URI failed.")))))

(provide 'to-data-uri)

;;; to-data-uri.el ends here
